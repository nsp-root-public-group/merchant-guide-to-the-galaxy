package com.srinivas.conversion;

import com.srinivas.data.GalacticTradeDataUtils;
import com.srinivas.exceptions.InvalidRomanException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
class RomanToDecimalConvertTest {

    @Test
    void shouldConvertSuccessfully1() throws InvalidRomanException {
        assertEquals(1903, GalacticTradeDataUtils.romanToDecimal("MCMIII"));
    }

    @Test
    void shouldConvertSuccessfully2() throws InvalidRomanException {
        assertEquals(3900, GalacticTradeDataUtils.romanToDecimal("MMMCM"));
    }

    @Test()
    void shouldFaiToConvert1() {
        Assertions.assertThrows(InvalidRomanException.class, () -> GalacticTradeDataUtils.romanToDecimal("XXXX"));
    }
    @Test()
    void shouldFaiToConvert2(){
        Assertions.assertThrows(InvalidRomanException.class, () -> GalacticTradeDataUtils.romanToDecimal("MMMIM"));
    }
}
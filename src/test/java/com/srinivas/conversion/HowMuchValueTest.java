package com.srinivas.conversion;

import com.srinivas.exceptions.InvalidRomanException;
import com.srinivas.preparedata.PrepareGalacticTradeData;
import com.srinivas.preparedata.PrepareGalacticUnitsToRomanData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HowMuchValueTest {

    GalaxyTradeConversion galaxyTradeConversion;
    PrepareGalacticTradeData prepareGalacticTradeData;
    List<String> validGalaxyTradeQuestion;
    List<String> invalidGalaxyTradeQuestion;
    @BeforeEach
    public void setup() throws InvalidRomanException {
        galaxyTradeConversion = new HowMuchValue();
        prepareGalacticTradeData = new PrepareGalacticUnitsToRomanData();
        validGalaxyTradeQuestion = Arrays.asList("how much is pish tegj glob glob ?".trim().split("\\s+"));
        invalidGalaxyTradeQuestion = Arrays.asList("how much is glob glob pish tegj glob glob ?".trim().split("\\s+"));
        populateGalacticUnitsToRomanData();
    }
    private void populateGalacticUnitsToRomanData() throws InvalidRomanException {
        prepareGalacticTradeData.prepareInputData(Arrays.asList("glob is I".trim().split("\\s+")));
        prepareGalacticTradeData.prepareInputData(Arrays.asList("prok is V".trim().split("\\s+")));
        prepareGalacticTradeData.prepareInputData(Arrays.asList("pish is X".trim().split("\\s+")));
        prepareGalacticTradeData.prepareInputData(Arrays.asList("tegj is L".trim().split("\\s+")));
    }
    @Test
    public void ShouldSucceedWhenValidInputTest() throws InvalidRomanException {
        assertEquals("pish tegj glob glob is 42" ,galaxyTradeConversion.convertFromGivenGalacticTradeInputs(validGalaxyTradeQuestion));
    }

    @Test
    public void shouldFailWhenInvalidInputTest() {
        Assertions.assertThrows(InvalidRomanException.class, () -> galaxyTradeConversion.convertFromGivenGalacticTradeInputs(invalidGalaxyTradeQuestion));
    }
}
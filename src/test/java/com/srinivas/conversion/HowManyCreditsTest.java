package com.srinivas.conversion;

import com.srinivas.exceptions.InvalidRomanException;
import com.srinivas.preparedata.PrepareGalacticTradeData;
import com.srinivas.preparedata.PrepareGalacticUnitsToRomanData;
import com.srinivas.preparedata.PrepareMetalsUnitPriceData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HowManyCreditsTest {

    GalaxyTradeConversion galaxyTradeConversion;
    PrepareGalacticTradeData prepareGalacticUnitsToRomanData;
    PrepareGalacticTradeData prepareMetalsUnitPriceData;

    List<String> validGalaxyTradeQuestion;
    List<String> invalidGalaxyTradeQuestion;
    @BeforeEach
    public void setup() throws InvalidRomanException {
        galaxyTradeConversion = new HowManyCredits();
        prepareGalacticUnitsToRomanData = new PrepareGalacticUnitsToRomanData();
        prepareMetalsUnitPriceData = new PrepareMetalsUnitPriceData();
        validGalaxyTradeQuestion = Arrays.asList("how many Credits is glob prok Gold ?".trim().split("\\s+"));
        invalidGalaxyTradeQuestion = Arrays.asList("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?".trim().split("\\s+"));
        populateGalacticUnitsToRomanData();
        populateMetalicUnitPriceData();
    }
    private void populateGalacticUnitsToRomanData() throws InvalidRomanException {
        prepareGalacticUnitsToRomanData.prepareInputData(Arrays.asList("glob is I".trim().split("\\s+")));
        prepareGalacticUnitsToRomanData.prepareInputData(Arrays.asList("prok is V".trim().split("\\s+")));
        prepareGalacticUnitsToRomanData.prepareInputData(Arrays.asList("pish is X".trim().split("\\s+")));
        prepareGalacticUnitsToRomanData.prepareInputData(Arrays.asList("tegj is L".trim().split("\\s+")));
    }

    private void populateMetalicUnitPriceData() throws InvalidRomanException {
        prepareMetalsUnitPriceData.prepareInputData(Arrays.asList("glob glob Silver is 34 Credits".trim().split("\\s+")));
        prepareMetalsUnitPriceData.prepareInputData(Arrays.asList("glob prok Gold is 57800 Credits".trim().split("\\s+")));
        prepareMetalsUnitPriceData.prepareInputData(Arrays.asList("pish pish Iron is 3910 Credits".trim().split("\\s+")));
     }

    @Test
    public void ShouldSucceedWhenValidInputTest() throws InvalidRomanException {
        assertEquals("glob prok Gold is 57800 Credits" ,galaxyTradeConversion.convertFromGivenGalacticTradeInputs(validGalaxyTradeQuestion));
    }

    @Test
    public void shouldFailWhenInvalidInputTest() {
        Assertions.assertThrows(InvalidRomanException.class, () -> galaxyTradeConversion.convertFromGivenGalacticTradeInputs(invalidGalaxyTradeQuestion));
    }
}
package com.srinivas.exceptions;

public class InvalidRomanException extends Exception {
    public InvalidRomanException(String invalidRomanMessage) {
        super(invalidRomanMessage);
    }
}

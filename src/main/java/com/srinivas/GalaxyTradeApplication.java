package com.srinivas;

import com.srinivas.constants.Constants;
import com.srinivas.conversion.HowManyCredits;
import com.srinivas.conversion.HowMuchValue;
import com.srinivas.exceptions.InvalidRomanException;
import com.srinivas.preparedata.PrepareGalacticUnitsToRomanData;
import com.srinivas.preparedata.PrepareMetalsUnitPriceData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import static com.srinivas.constants.Constants.*;

public class GalaxyTradeApplication {

    public void readFileAndProcess(String fileName) {
        try (
                InputStream inputStream = getClass()
                        .getClassLoader().getResourceAsStream(fileName)) {
            if (inputStream == null)
                throw new IOException("File not found Exception.");
            readFileAndProcess(inputStream);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new GalaxyTradeApplication().readFileAndProcess(INPUT_FILE_NAME.toString());
    }

    public void readFileAndProcess(InputStream inputStream) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                List<String> inputLineSplitList = Arrays.asList(line.trim().split("\\s+"));
                int size = inputLineSplitList.size();

                if (size == 3) {// Input data to populate GalacticUnits to Roman mapping
                    new PrepareGalacticUnitsToRomanData().prepareInputData(inputLineSplitList);
                }else if (size >= 5 && line.endsWith(Constants.CREDITS.toString())) { // input data matches to populate metal unit prices
                    new PrepareMetalsUnitPriceData().prepareInputData(inputLineSplitList);
                } else if (line.startsWith(HOW_MUCH_IS.toString()) && line.endsWith(QUESTION_MARK.toString())) {
                    System.out.println(new HowMuchValue().convertFromGivenGalacticTradeInputs(inputLineSplitList));
                } else if (line.startsWith(HOW_MANY_CREDITS_IS.toString()) && line.endsWith(QUESTION_MARK.toString())) {
                    System.out.println(new HowManyCredits().convertFromGivenGalacticTradeInputs(inputLineSplitList));
                } else {
                    System.out.println(INVALID_INPUT.toString());
                }
            }
        } catch (InvalidRomanException e) {
            System.out.println(INVALID_INPUT.toString());
        }
    }


}

package com.srinivas.constants;

public enum Constants {

    INVALID_INPUT("I have no idea what you are talking about"),
    CREDITS("Credits"),
    HOW_MUCH_IS("how much is"),
    HOW_MANY_CREDITS_IS ("how many Credits is"),
    INPUT_FILE_NAME("input.text"),
    QUESTION_MARK("?");

    private final String message;

    Constants(final String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}

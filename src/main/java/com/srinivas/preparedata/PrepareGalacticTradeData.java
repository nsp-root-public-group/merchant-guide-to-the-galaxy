package com.srinivas.preparedata;

import com.srinivas.exceptions.InvalidRomanException;

import java.util.List;

public interface PrepareGalacticTradeData {
    void prepareInputData(List<String> inputSplitLine) throws InvalidRomanException;
}

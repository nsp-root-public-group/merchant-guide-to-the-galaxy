package com.srinivas.preparedata;

import com.srinivas.data.GalacticTradeData;

import java.util.List;

public class PrepareGalacticUnitsToRomanData implements PrepareGalacticTradeData {

    @Override
    public void prepareInputData(List<String> inputSplitLine) {
        fillInterGalacticUnitsToRomanNumericalMap(inputSplitLine);
    }
    private void fillInterGalacticUnitsToRomanNumericalMap(List<String> inputSplitLine) {
        // Example: "glob is I" the first and last is our concern
        GalacticTradeData.getInterGalacticUnitsToRomanNumericalMap().put(inputSplitLine.get(0), inputSplitLine.get(2));
    }
}


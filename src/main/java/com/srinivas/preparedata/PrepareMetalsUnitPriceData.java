package com.srinivas.preparedata;

import com.srinivas.data.GalacticTradeData;
import com.srinivas.data.GalacticTradeDataUtils;
import com.srinivas.exceptions.InvalidRomanException;

import java.util.List;

public class PrepareMetalsUnitPriceData implements PrepareGalacticTradeData {

    @Override
    public void prepareInputData(List<String> inputSplitLine) throws InvalidRomanException {
        fillMetalUnitValueMap(inputSplitLine, inputSplitLine.size());
    }

    private void fillMetalUnitValueMap(List<String> inputSplitLine, int size) throws InvalidRomanException {
        // Example: glob glob Silver is 34 Credits
        // Extract Credit from inut string, the last but one is the credit, so its size - 2
        double credits = Double.parseDouble(inputSplitLine.get(size - 2));
        // Metal position in input string is 4th from last, so Ssize - 4, and galactic strings in input string are from 0 to size - 4
        GalacticTradeData.getMetalUnitValueMap().put(inputSplitLine.get(size - 4), calculateMetalUnitValue(inputSplitLine.subList(0, size-4), credits));
    }

    private double calculateMetalUnitValue(List<String> inputSplitLine, double credits) throws InvalidRomanException {
        // Unit Price of Metal = Credit / decimal value of converted roman from galactic strings
        String totalRomanNumericValueOfGalacticUnits = GalacticTradeDataUtils.getRomanStringFromGalacticUnits(inputSplitLine);
        return credits / GalacticTradeDataUtils.romanToDecimal(totalRomanNumericValueOfGalacticUnits);
    }
}

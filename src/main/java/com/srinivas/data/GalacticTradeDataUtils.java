package com.srinivas.data;

import com.srinivas.exceptions.InvalidRomanException;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.srinivas.constants.Constants.INVALID_INPUT;

public class GalacticTradeDataUtils {

    public static int romanToDecimal(String s) throws InvalidRomanException {
        if (s == null || s.isEmpty() || !s.toUpperCase().matches("^(M{0,3})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$"))
            throw new InvalidRomanException(INVALID_INPUT.toString());

        final Matcher matcher = Pattern.compile("M|CM|D|CD|C|XC|L|XL|X|IX|V|IV|I").matcher(s);
        final int[] decimalValues = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
        final String[] romanNumerals = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
        int result = 0;

        while (matcher.find())
            for (int i = 0; i < romanNumerals.length; i++)
                if (romanNumerals[i].equals(matcher.group(0)))
                    result += decimalValues[i];

        return result;
    }

    public static String getRomanStringFromGalacticUnits(List<String> inputSplitLine) {
        return inputSplitLine.stream()
                .map(galacticUnit -> GalacticTradeData.getInterGalacticUnitsToRomanNumericalMap().get(galacticUnit))
                .collect(Collectors.joining());
    }

    public static int calculateTheDecimalValueOfGalacticUnits(List<String> inputSplitLine) throws InvalidRomanException {
        String totalRomanNumericValueOfGalacticUnits = getRomanStringFromGalacticUnits(inputSplitLine);
        return GalacticTradeDataUtils.romanToDecimal(totalRomanNumericValueOfGalacticUnits);
    }
}

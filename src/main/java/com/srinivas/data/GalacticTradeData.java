package com.srinivas.data;

import java.util.HashMap;
import java.util.Map;

public class GalacticTradeData {
    private static Map<String, String> interGalacticUnitsToRomanNumericalMap = new HashMap<>();
    private static Map<String, Double> metalUnitValueMap = new HashMap<>();

    public static Map<String, String> getInterGalacticUnitsToRomanNumericalMap() {
        return interGalacticUnitsToRomanNumericalMap;
    }
    public static Map<String, Double> getMetalUnitValueMap() {
        return metalUnitValueMap;
    }
}

package com.srinivas.conversion;

import com.srinivas.data.GalacticTradeData;
import com.srinivas.exceptions.InvalidRomanException;

import java.util.List;

import static com.srinivas.constants.Constants.CREDITS;
import static com.srinivas.data.GalacticTradeDataUtils.calculateTheDecimalValueOfGalacticUnits;

public class HowManyCredits implements GalaxyTradeConversion {

    @Override
    public String convertFromGivenGalacticTradeInputs(List<String> inputSplitLine) throws InvalidRomanException {
        // example: how many Credits is glob prok Gold
        // skip first 4 to be left with galactics and metals
        List<String> listOfGalacticUnitsAndMetal = inputSplitLine.subList(4, inputSplitLine.size()-1);
        return responseString(listOfGalacticUnitsAndMetal, calculateCredits(listOfGalacticUnitsAndMetal));
    }


    private int calculateCredits(List<String> inputSplitLine) throws InvalidRomanException {
        // unit values of metal * decimal value of Galatics
        // Eg: glob prok Gold: Last one is metal rest all are galactic words.
        double credit = calculateTheDecimalValueOfGalacticUnits(inputSplitLine.subList(0, inputSplitLine.size() - 1))
                * GalacticTradeData.getMetalUnitValueMap().get(inputSplitLine.get(inputSplitLine.size() - 1));
        return (int) credit;
    }
    private String responseString(List<String> listOfGalacticUnitsAndMetal, int credits) {
       return String.join(" ", listOfGalacticUnitsAndMetal)
                + " is "
                + credits
                +" "+CREDITS.toString();
    }
}

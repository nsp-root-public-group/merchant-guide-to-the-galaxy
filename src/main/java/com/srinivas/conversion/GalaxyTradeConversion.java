package com.srinivas.conversion;

import com.srinivas.exceptions.InvalidRomanException;

import java.util.List;

public interface GalaxyTradeConversion {
    String convertFromGivenGalacticTradeInputs(List<String> inputTradeValues) throws InvalidRomanException;
}

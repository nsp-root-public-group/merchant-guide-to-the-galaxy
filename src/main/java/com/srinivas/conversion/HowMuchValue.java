package com.srinivas.conversion;

import com.srinivas.exceptions.InvalidRomanException;

import java.util.List;

import static com.srinivas.data.GalacticTradeDataUtils.calculateTheDecimalValueOfGalacticUnits;

public class HowMuchValue implements GalaxyTradeConversion {
    @Override
    public String convertFromGivenGalacticTradeInputs(List<String> inputSplitLine) throws InvalidRomanException {
        //how much is pish tegj glob glob ?
        // ignore first 3 and last '?', considering remaining are galactics
        List<String> listOfGalacticUnits = inputSplitLine.subList(3, inputSplitLine.size()-1);
        return responseString(listOfGalacticUnits, calculateTheDecimalValueOfGalacticUnits(listOfGalacticUnits));
    }
    private String responseString(List<String> listOfGalacticUnits, int decimalValueOfGalacticUnits) {
        return String.join(" ", listOfGalacticUnits)
                + " is "
                + decimalValueOfGalacticUnits;
    }
}
